﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Devices.Bluetooth.Rfcomm;
using Windows.Devices.Enumeration;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Networking.Sockets;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace BlueTooth_demo
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {

        private StreamSocket _socket;
        private RfcommDeviceService _service;
        private int _fire_or_not = 0;

        public MainPage()
        {
            this.InitializeComponent();
        }

        private async void btnSend_Click(object sender, RoutedEventArgs e)
        {
            int dummy;

            if (!int.TryParse(tbInput.Text, out dummy))
            {
                tbError.Text = "Invalid input";
            }

            var noOfCharsSent = await Send(tbInput.Text);

            if (noOfCharsSent != 0)
            {
                tbError.Text = noOfCharsSent.ToString();
            }
        }

        //send message
        private async Task<uint> Send(string msg)
        {
            tbError.Text = string.Empty;

            try
            {
                var writer = new DataWriter(_socket.OutputStream);

                writer.WriteString(msg);

                // Launch an async task to complete the write operation
                var store = writer.StoreAsync().AsTask();

                return await store;
            }
            catch (Exception ex)
            {
                tbError.Text = ex.Message;

                return 0;
            }
        }

        //connect
        private async void btnConnect_Click(object sender, RoutedEventArgs e)
        {
            tbError.Text = string.Empty;
            String deviceName = tbDeviceName.Text;

            try
            {
                var devices = await DeviceInformation.FindAllAsync(RfcommDeviceService.GetDeviceSelector(RfcommServiceId.SerialPort));

                var device = devices.Single(x => x.Name == deviceName);
                Debug.WriteLine(device);

                _service = await RfcommDeviceService.FromIdAsync(device.Id);
                _socket = new StreamSocket();

                await _socket.ConnectAsync( _service.ConnectionHostName,_service.ConnectionServiceName,SocketProtectionLevel.BluetoothEncryptionAllowNullAuthentication);
            }
            catch (Exception ex)
            {
                tbError.Text = ex.Message;
            }
        }

        //disconnect
        private async void btnDisconnect_Click(object sender, RoutedEventArgs e)
        {
            tbError.Text = string.Empty;

            try
            {
                await _socket.CancelIOAsync();
                _socket.Dispose();
                _socket = null;
                _service.Dispose();
                _service = null;
            }
            catch (Exception ex)
            {
                tbError.Text = ex.Message;
            }
        }

        //movements ----------
        private async void btnUp_Click(object sender, RoutedEventArgs e)
        {
            var noOfCharsSent = await Send("8");

            if (noOfCharsSent != 0)
            {
                tbError.Text = noOfCharsSent.ToString();
            }
        }

        private async void btnLeft_Click(object sender, RoutedEventArgs e)
        {
            var noOfCharsSent = await Send("4");

            if (noOfCharsSent != 0)
            {
                tbError.Text = noOfCharsSent.ToString();
            }
        }

        private async void btnRight_Click(object sender, RoutedEventArgs e)
        {
            var noOfCharsSent = await Send("6");

            if (noOfCharsSent != 0)
            {
                tbError.Text = noOfCharsSent.ToString();
            }
        }

        //lights
        private async void btnFire_Click(object sender, RoutedEventArgs e)
        {
            if(this._fire_or_not == 0)
            {
                this._fire_or_not = 1;
                btnFire.Content = "0";
            }
            else
            {
                this._fire_or_not = 0;
                btnFire.Content = "1";
            }

            var noOfCharsSent = await Send(this._fire_or_not.ToString());

            if (noOfCharsSent != 0)
            {
                tbError.Text = noOfCharsSent.ToString();
            }
        }

        //buzz
        private async void btnBuzz_Click(object sender, RoutedEventArgs e)
        {
            var noOfCharsSent = await Send("3");

            if (noOfCharsSent != 0)
            {
                tbError.Text = noOfCharsSent.ToString();
            }
        }
    }
}
